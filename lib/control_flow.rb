# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str = str.chars.select { |char| char.upcase == char }.join
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  substring = str
  substring = substring[1..-2] until substring.length <= 2
  substring
end

# Return the number of vowels in a string.
def num_vowels(str)
  str.scan(/[aeiou]/i).count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  return num if num <= 1
  num * factorial(num - 1)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  arr[0...-1].inject("") { |str, el| str + el.to_s + separator } + arr.last.to_s
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  str.chars.map.with_index do |char, idx|
    idx.even? ? char.downcase : char.upcase
  end.join
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  str.split.map { |word| word.length > 4 ? word.reverse : word }.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  1.upto(n).map do |num|
    fizz_or_buzz(num)
  end
end

def fizz_or_buzz(num)
  return "fizzbuzz"if num % 3 == 0 && num % 5 == 0
  return "fizz" if num % 3 == 0
  return "buzz" if num % 5 == 0
  num
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  reversed = []
  arr.each { |el| reversed.unshift(el) }
  reversed
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num == 1
  (2...num).none? { |n| num % n == 0 }
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  1.upto(num).select { |n| num % n == 0 }
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors(num).select { |factor| prime?(factor) }
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).count
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  less = arr.one?(&:odd?) ? arr.detect(&:odd?) : arr.detect(&:even?)
end
